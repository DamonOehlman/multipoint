// req: events

var _slice = Array.prototype.slice;

//= core/pipeline

function _arr(src) {
    var props = _slice.call(arguments, 1),
        propCount = props.length,
        output = [];
    
    for (var ii = src.length; ii--; ) {
        var el = [];
        
        for (var propIdx = propCount; propIdx--; ) {
            el[propIdx] = src[ii][props[propIdx]];
        }
        
        output[ii] = el;
    }
    
    return output;
}

function _obj(src) {
    var props = _slice.call(arguments, 1),
        propCount = props.length,
        output = [];
        
    for (var ii = src.length; ii--; ) {
        var el = {};
        
        for (var propIdx = propCount; propIdx--; ) {
            el[props[propIdx]] = src[ii][propIdx];
        }
        
        output[ii] = el;
    }
    
    return output;
}

function _translate(src) {
    var propCount = (arguments.length - 1) / 2,
        propsIn, propsOut,
        output = [];
    
    if (propCount !== (propCount | 0)) {
        throw new Error('Translation requires the same number of source properties as destination properties');
    }
    
    // initialise props in and out
    propsIn = _slice.call(arguments, 1, propCount + 1);
    propsOut = _slice.call(arguments, propCount + 1);
    
    // iterate through the items and translate from one item to the other
    for (var ii = src.length; ii--; ) {
        var el = {};
        
        // iterate through the properties and map
        for (var propIdx = propCount; propIdx--; ) {
            el[propsOut[propIdx]] = src[ii][propsIn[propIdx]];
        }
        
        output[ii] = el;
    }
    
    return output;
}

function multipoint(items, opts) {
    return new Pipeline(items, opts);
}

/* include core analysers */

//= analysers/min
//= analysers/max
//= analysers/bounds

/* include helpers */

//= helpers/wrap

/* include core modifiers */

multipoint.arr = _arr;
multipoint.obj = _obj;
multipoint.translate = _translate;

multipoint.Pipeline = Pipeline;