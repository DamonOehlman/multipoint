// ## multipoint.wrap
// The wrap helper is used to wrap a function that takes an array as an argument
// and returns another array as the return value.  This makes the process of using
// existing functions designed to work with arrays simple to integrate with multipoint.
multipoint.wrap = function(fn) {
    return function(items) {
        this.ok(fn(items));
    };
};