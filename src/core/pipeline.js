function Pipeline(items) {
    // call the inherited constructor
    events.EventEmitter.call(this);
    
    // initialise the items
    this.items = items || [];
    
    // initialise the processes
    this.processes = [];
    this.processIndex = -1;
    
    // initialise the active state
    this.active = false;
    this.triggerTimer = 0;
    
    // initialise event handlers
    this._initEvents();
}

Pipeline.prototype = new events.EventEmitter();

Pipeline.prototype.error = function(error) {
    if (! (error instanceof Error)) {
        error = new Error(error);
    }
    
    this.emit('error', error);
    return this;
};

Pipeline.prototype.have = function(property, calculator) {
    // check to see whether we have the required property, if not, inject the multipoint analyser of the same name
    if (typeof this[property] != 'undefined') {
        return true;
    }
    
    // queue the method for immediate execution
    this._inject(calculator || multipoint[property]);
    
    // not yet available so return false
    return false;
};

Pipeline.prototype.ok = function(items) {
    this.emit('next', items);
    return this;
};

Pipeline.prototype.queue = function(fn) {
    if (typeof fn == 'function') {
        this.processes.push(fn);
        this.start();
    }
    else {
        throw new Error('A processor function is required for a \'queue\' call');
    }
    
    return this;
};

Pipeline.prototype.start = function() {
    if (! this.active) {
        var pipeline = this;

        // flag as active
        this.active = true;
        
        clearTimeout(this.triggerTimer);
        this.triggerTimer = setTimeout(function() {
            pipeline.processIndex = -1;
            pipeline.emit('next');
        }, 0);
    }
    
    return this;
};

Pipeline.prototype._inject = function(fn) {
    var pipeline = this;
    
    // if we don't have an active fork of the pipeline, then create one
    if (! this.fork) { 
        var fork = this.fork = new Pipeline(this.items);
        fork.once('done', function() {
            pipeline.fork = null;
            
            // copy across items in the forked pipeline across to the current pipeline
            for (var key in fork) {
                if (fork.hasOwnProperty(key) && typeof pipeline[key] == 'undefined') {
                    pipeline[key] = fork[key];
                }
            }
            
            // try the current process again
            pipeline._next(pipeline.processes[pipeline.processIndex]);
        });
    }
    
    this.fork.queue(fn);
};

Pipeline.prototype._initEvents = function() {
    var pipeline = this;
    
    this.on('next', function(items) {
        if (typeof items != 'undefined') {
            pipeline.items = items;
        }
        
        // increment the process index
        pipeline.processIndex += 1;
        
        if (pipeline.processIndex >= pipeline.processes.length) {
            pipeline.emit('done', items);
        }
        else {
            pipeline._next(pipeline.processes[pipeline.processIndex]);
        }
    });
    
    this.on('done', function() {
        pipeline.active = false;
        pipeline.processIndex = -1;
    });
};

Pipeline.prototype._next = function(fn) {
    // convert the items
    fn.call(this, this.items);
};