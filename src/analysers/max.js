multipoint.max = function(items) {
    // reset the min values
    this.max = [];
    
    // iterate through the items
    for (var ii = items.length; ii--; ) {
        for (var jj = items[ii].length; jj--; ) {
            var val = items[ii][jj];
            
            if (typeof this.max[jj] == 'undefined' || val > this.max[jj]) {
                this.max[jj] = val;
            }
        }
    }
    
    this.ok(items);
};