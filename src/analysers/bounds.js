multipoint.bounds = function(items) {
    if (this.have('min') && this.have('max')) {
        this.bounds = [this.min, this.max];
        this.ok();
    }
};