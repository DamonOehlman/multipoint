multipoint.min = function(items) {
    // reset the min values
    this.min = [];
    
    // iterate through the items
    for (var ii = items.length; ii--; ) {
        for (var jj = items[ii].length; jj--; ) {
            var val = items[ii][jj];
            
            if (typeof this.min[jj] == 'undefined' || val < this.min[jj]) {
                this.min[jj] = val;
            }
        }
    }
    
    this.ok(items);
};