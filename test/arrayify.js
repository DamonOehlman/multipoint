var multipoint = require('../multipoint'),
    expect = require('expect.js'),
    item1 = { x: 10, y: 20 },
    item2 = { x: 20, y: 40 },
    item3 = { x: 43, y: 67 };

describe('object array to 2d array tests', function() {
    it('simple 2 element array', function() {
        var src = [item1, item2],
            output = multipoint.arr(src, 'x', 'y');

        expect(output).to.have.length(2);
        expect(output[0][0]).to.equal(10);
        expect(output[0][1]).to.equal(20);
        expect(output[1][0]).to.equal(20);
        expect(output[1][1]).to.equal(40);
    });
    
    it('simple 3 element array', function() {
        var src = [item1, item2, item3],
            output = multipoint.arr(src, 'x', 'y');

        expect(output).to.have.length(3);
        expect(output[0][0]).to.equal(10);
        expect(output[0][1]).to.equal(20);
        expect(output[1][0]).to.equal(20);
        expect(output[1][1]).to.equal(40);
        expect(output[2][0]).to.equal(43);
        expect(output[2][1]).to.equal(67);
    });
});