var multipoint = require('../multipoint'),
    expect = require('expect.js'),
    item1 = { x: 10, y: 20 },
    item2 = { x: 20, y: 40 },
    item3 = { x: 43, y: 67 };

describe('object array property translation', function() {
    it('simple 2 element array', function() {
        var output = multipoint.translate([item1, item2], 'x', 'y', 'X', 'Y');

        expect(output).to.have.length(2);
        expect(output[0].X).to.equal(10);
        expect(output[0].Y).to.equal(20);
        expect(output[1].X).to.equal(20);
        expect(output[1].Y).to.equal(40);
    });
    
    it('simple 3 element array', function() {
        var output = multipoint.translate([item1, item2, item3], 'x', 'y', 'X', 'Y');

        expect(output).to.have.length(3);
        expect(output[0].X).to.equal(10);
        expect(output[0].Y).to.equal(20);
        expect(output[1].X).to.equal(20);
        expect(output[1].Y).to.equal(40);
        expect(output[2].X).to.equal(43);
        expect(output[2].Y).to.equal(67);
    });
});