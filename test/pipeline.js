var multipoint = require('../multipoint'),
    expect = require('expect.js'),
    data = [
        [10, 40],
        [20, 20],
        [25, 67],
        [43, 50]
    ];
    
    
describe('pipeline tests', function() {
    it('should be able to create a new pipeline', function() {
        var pipeline = multipoint(data);
        
        expect(pipeline).to.be.ok();
        expect(pipeline instanceof multipoint.Pipeline).to.be.ok();
    });

    it('should be able to run a noop function on the pipeline', function(done) {
        var pipeline = multipoint(data);
        
        pipeline.once('done', function() {
            expect(this.items).to.be.ok();
            expect(this.items).to.have.length(4);
            
            done();
        });
        
        pipeline.queue(function(items) {
            this.ok(items);
        });
    });
    
    it('should be able to get the min values for the pipeline values', function(done) {
        var pipeline = multipoint(data);
        
        pipeline.once('done', function() {
            expect(this.min).to.be.ok();
            expect(this.min).to.have.length(2);
            expect(this.min[0]).to.equal(10);
            expect(this.min[1]).to.equal(20);
            
            done();
        });
        
        expect(pipeline.min).to.not.be.ok();
        pipeline.queue(multipoint.min);
    });
    
    it('should be able to get the max values for the pipeline values', function(done) {
        var pipeline = multipoint(data);
        
        pipeline.once('done', function() {
            expect(this.max).to.be.ok();
            expect(this.max).to.have.length(2);
            expect(this.max[0]).to.equal(43);
            expect(this.max[1]).to.equal(67);
            
            done();
        });
        
        expect(pipeline.max).to.not.be.ok();
        pipeline.queue(multipoint.max);
    });
    
    it('should be able to get the bounds for the pipeline values', function(done) {
        var pipeline = multipoint(data);
        
        pipeline.once('done', function(items) {
            expect(this.bounds).to.be.ok();
            expect(this.bounds).to.have.length(2);
            expect(this.bounds[0][0]).to.equal(10);
            expect(this.bounds[0][1]).to.equal(20);
            expect(this.bounds[1][0]).to.equal(43);
            expect(this.bounds[1][1]).to.equal(67);
            
            done();
        });
        
        pipeline.queue(multipoint.bounds);
    });
});