var multipoint = require('../multipoint'),
    expect = require('expect.js'),
    item1 = { x: 10, y: 20 },
    item2 = { x: 20, y: 40 },
    item3 = { x: 43, y: 67 };

describe('2d array tests to object array tests', function() {
    it('simple 2 element array', function() {
        var output = multipoint.obj([ [10, 20], [20, 40]], 'x', 'y');

        expect(output).to.have.length(2);
        expect(output[0]).to.eql(item1);
        expect(output[1]).to.eql(item2);
    });
    
    it('simple 3 element array', function() {
        var output = multipoint.obj([ [10, 20], [20, 40], [43, 67]], 'x', 'y');

        expect(output).to.have.length(3);
        expect(output[0]).to.eql(item1);
        expect(output[1]).to.eql(item2);
        expect(output[2]).to.eql(item3);
    });
});