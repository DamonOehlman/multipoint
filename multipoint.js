/* ~multipoint~
 * 
 * Multipoint Translation and Operations Library
 * 
 * -meta---
 * version:    0.1.0
 * builddate:  2012-10-30T04:31:12.343Z
 * generator:  interleave@0.5.23
 * 
 * 
 * 
 */ 

// umdjs returnExports pattern: https://github.com/umdjs/umd/blob/master/returnExports.js
(function (root, factory) {
    if (typeof exports === 'object') {
        module.exports = factory(require('events'));
    } else if (typeof define === 'function' && define.amd) {
        define(['events'], factory);
    } else {
        root['multipoint'] = factory(root['events']);
    }
}(this, function (events) {
    
    var _slice = Array.prototype.slice;
    
    function Pipeline(items) {
        // call the inherited constructor
        events.EventEmitter.call(this);
        
        // initialise the items
        this.items = items || [];
        
        // initialise the processes
        this.processes = [];
        this.processIndex = -1;
        
        // initialise the active state
        this.active = false;
        this.triggerTimer = 0;
        
        // initialise event handlers
        this._initEvents();
    }
    
    Pipeline.prototype = new events.EventEmitter();
    
    Pipeline.prototype.error = function(error) {
        if (! (error instanceof Error)) {
            error = new Error(error);
        }
        
        this.emit('error', error);
        return this;
    };
    
    Pipeline.prototype.have = function(property, calculator) {
        // check to see whether we have the required property, if not, inject the multipoint analyser of the same name
        if (typeof this[property] != 'undefined') {
            return true;
        }
        
        // queue the method for immediate execution
        this._inject(calculator || multipoint[property]);
        
        // not yet available so return false
        return false;
    };
    
    Pipeline.prototype.ok = function(items) {
        this.emit('next', items);
        return this;
    };
    
    Pipeline.prototype.queue = function(fn) {
        if (typeof fn == 'function') {
            this.processes.push(fn);
            this.start();
        }
        else {
            throw new Error('A processor function is required for a \'queue\' call');
        }
        
        return this;
    };
    
    Pipeline.prototype.start = function() {
        if (! this.active) {
            var pipeline = this;
    
            // flag as active
            this.active = true;
            
            clearTimeout(this.triggerTimer);
            this.triggerTimer = setTimeout(function() {
                pipeline.processIndex = -1;
                pipeline.emit('next');
            }, 0);
        }
        
        return this;
    };
    
    Pipeline.prototype._inject = function(fn) {
        var pipeline = this;
        
        // if we don't have an active fork of the pipeline, then create one
        if (! this.fork) { 
            var fork = this.fork = new Pipeline(this.items);
            fork.once('done', function() {
                pipeline.fork = null;
                
                // copy across items in the forked pipeline across to the current pipeline
                for (var key in fork) {
                    if (fork.hasOwnProperty(key) && typeof pipeline[key] == 'undefined') {
                        pipeline[key] = fork[key];
                    }
                }
                
                // try the current process again
                pipeline._next(pipeline.processes[pipeline.processIndex]);
            });
        }
        
        this.fork.queue(fn);
    };
    
    Pipeline.prototype._initEvents = function() {
        var pipeline = this;
        
        this.on('next', function(items) {
            if (typeof items != 'undefined') {
                pipeline.items = items;
            }
            
            // increment the process index
            pipeline.processIndex += 1;
            
            if (pipeline.processIndex >= pipeline.processes.length) {
                pipeline.emit('done', items);
            }
            else {
                pipeline._next(pipeline.processes[pipeline.processIndex]);
            }
        });
        
        this.on('done', function() {
            pipeline.active = false;
            pipeline.processIndex = -1;
        });
    };
    
    Pipeline.prototype._next = function(fn) {
        // convert the items
        fn.call(this, this.items);
    };
    
    function _arr(src) {
        var props = _slice.call(arguments, 1),
            propCount = props.length,
            output = [];
        
        for (var ii = src.length; ii--; ) {
            var el = [];
            
            for (var propIdx = propCount; propIdx--; ) {
                el[propIdx] = src[ii][props[propIdx]];
            }
            
            output[ii] = el;
        }
        
        return output;
    }
    
    function _obj(src) {
        var props = _slice.call(arguments, 1),
            propCount = props.length,
            output = [];
            
        for (var ii = src.length; ii--; ) {
            var el = {};
            
            for (var propIdx = propCount; propIdx--; ) {
                el[props[propIdx]] = src[ii][propIdx];
            }
            
            output[ii] = el;
        }
        
        return output;
    }
    
    function _translate(src) {
        var propCount = (arguments.length - 1) / 2,
            propsIn, propsOut,
            output = [];
        
        if (propCount !== (propCount | 0)) {
            throw new Error('Translation requires the same number of source properties as destination properties');
        }
        
        // initialise props in and out
        propsIn = _slice.call(arguments, 1, propCount + 1);
        propsOut = _slice.call(arguments, propCount + 1);
        
        // iterate through the items and translate from one item to the other
        for (var ii = src.length; ii--; ) {
            var el = {};
            
            // iterate through the properties and map
            for (var propIdx = propCount; propIdx--; ) {
                el[propsOut[propIdx]] = src[ii][propsIn[propIdx]];
            }
            
            output[ii] = el;
        }
        
        return output;
    }
    
    function multipoint(items, opts) {
        return new Pipeline(items, opts);
    }
    
    /* include core analysers */
    
    multipoint.min = function(items) {
        // reset the min values
        this.min = [];
        
        // iterate through the items
        for (var ii = items.length; ii--; ) {
            for (var jj = items[ii].length; jj--; ) {
                var val = items[ii][jj];
                
                if (typeof this.min[jj] == 'undefined' || val < this.min[jj]) {
                    this.min[jj] = val;
                }
            }
        }
        
        this.ok(items);
    };
    multipoint.max = function(items) {
        // reset the min values
        this.max = [];
        
        // iterate through the items
        for (var ii = items.length; ii--; ) {
            for (var jj = items[ii].length; jj--; ) {
                var val = items[ii][jj];
                
                if (typeof this.max[jj] == 'undefined' || val > this.max[jj]) {
                    this.max[jj] = val;
                }
            }
        }
        
        this.ok(items);
    };
    multipoint.bounds = function(items) {
        if (this.have('min') && this.have('max')) {
            this.bounds = [this.min, this.max];
            this.ok();
        }
    };
    
    /* include helpers */
    
    // ## multipoint.wrap
    // The wrap helper is used to wrap a function that takes an array as an argument
    // and returns another array as the return value.  This makes the process of using
    // existing functions designed to work with arrays simple to integrate with multipoint.
    multipoint.wrap = function(fn) {
        return function(items) {
            this.ok(fn(items));
        };
    };
    
    /* include core modifiers */
    
    multipoint.arr = _arr;
    multipoint.obj = _obj;
    multipoint.translate = _translate;
    
    multipoint.Pipeline = Pipeline;
    
    return typeof multipoint != 'undefined' ? multipoint : undefined;
}));