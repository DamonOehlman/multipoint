# Multipoint

Translation to and from object arrays representing nD data.  Useful for integrating with libraries that perform geometric or geospatial operations but designed to work with a different syntax for primitive objects.

<a href="http://travis-ci.org/#!/DamonOehlman/multipoint"><img src="https://secure.travis-ci.org/DamonOehlman/multipoint.png" alt="Build Status"></a>

Multipoint considers a 2-dimensional array its native format. For instance the following is an example of 2d data:

```js
var data = [
    [10, 20],
    [20, 40],
    [43, 67],
    [25, 50]
];
```

And this might be an example of 3d data:

```js
var data = [
    [1, 2, 3],
    [4, 5, 6]
];
```

## Conversion Functions

### "Arrayify"

It's unlikely that you will be working with a library that deals with this data in this way, however, so if you want to make use of the native functionality (which is coming) then you should first convert your object array into a multipoint array.  This can be done with the `multipoint.arr` function:

```js
var sourceData = [
    { x: 10, y: 20 },
    { x: 20, y: 40 },
    { x: 43, y: 67 },
    { x: 25, y: 50 }
];

var data = multipoint.arr(sourceData, 'x', 'y');
```

In the example above, we can see that we start with an array containing 4 objects with `x` and `y` property values.  We use the `multipoint.arr` function to convert this into a 2-dimensional array.  The first argument to the `arr` function is always the source data array, followed by the names of the properties that contain data for the other dimensions of the array (in the above case, x & y).

Now that you've got a 2-dimensional array, there is a lot you can do with it.

### "Objectify"

To objectify an array, you would use the `multipoint.obj` function which behaves in a similar, but reverse way to the `multipoint.arr` call:

```js
var data = [
    [10, 20],
    [20, 40],
    [43, 67],
    [25, 50]
];

var output = multipoint.obj(data, 'x', 'y');
```

After this has run, the output will be the same as what was first submitted to the `multipoint.arr` function in the "Arrayify" example above.

### Translate

Translate is another useful function for converting object arrays with properties of one name to another format.  Consider the case where you have a library that defines a Vector as an object with `x` and `y` properties.  You then find another library that performs vector operations on `X` and `Y` properties. 

Javascript being case sensitive will prevent you using library B with library A, however, using the `multipoint.translate` function you can potentially map simple object types from one type to another.  Prototypes will not be copied across, however, so if the library uses them you will probably need to implement some JS prototype magic to make everything work nicely.

For a simple example, see the [translate test](/DamonOehlman/multipoint/blob/master/test/translate.js).

## Blocking vs. Non-Blocking Operations

As many of you will know, Javascript is a single-threaded environment which means that any operations that take a significant amount of time will block the UI thread and cause lag in your application.  If you are using [NodeJS](http://nodejs.org/) or another server-side JS environment then blocking will affect your ability to serve other requests.

For these reason, it's important that we consider how to provide non-blocking implementations of operations on arrays containing a lot of data.


__NOTE:__ The conversion operations of `arr` and `obj` are blocking implementations which is why we recommend writing multipoint compatible algorithms wherever possible.

